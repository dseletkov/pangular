# pangular

_My first try on Angular_

#### Prerequisites
To run the application, you should have Node 8.* and npm 6.* installed.

#### Setting up
Run `npm i` to get the dependencies and you're ready to play.

#### Running
Application is run on `webpack-dev-server`, which you can start by

`npm run start`

command.

After that the app is available at <http://localhost:3003>.

#### Authentication
App uses [auth0](https://auth0.com/) for authentication, so no server is used.

You will have to sign up or login via Google.

#

#### Notes

Keyboard navigation doesn't work very well on Safari.

Looks the best on Firefox, from my point of view.

Didn't test on IE :(
