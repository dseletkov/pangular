import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './routes/home/home.component';
import { SearchComponent } from './routes/search/search.component';
import { QuestionComponent } from './routes/question/question.component';
import { LoginComponent } from './routes/login/login.component';
import { CallbackComponent } from './callback.component';

import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [
      AuthGuard
    ],
  },
  {
    path: 'search',
    component: SearchComponent,
    canActivate: [
      AuthGuard
    ],
  },
  {
    path: 'question/:id',
    component: QuestionComponent,
    canActivate: [
      AuthGuard
    ],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'callback',
    component: CallbackComponent,
  },
  {
    path: '**',
    redirectTo: '/',
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  providers: [ AuthGuard ],
  exports: [ RouterModule ],
})
export class AppRoutingModule {}