import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { HomeComponent } from './routes/home/home.component';
import { SearchComponent } from './routes/search/search.component';
import { QuestionComponent } from './routes/question/question.component';
import { LoginComponent } from './routes/login/login.component';

import { QuestionListComponent } from './components/QuestionList/question-list.component';
import { CallbackComponent } from './callback.component';

import { SearchService } from './routes/search/search.service';
import { AnswerService } from './routes/question/answer.service';
import { AuthService } from './auth/auth.service';

@NgModule({
  imports: [
      BrowserModule,
      FormsModule,
      HttpClientModule,
      NgbModule,
      AppRoutingModule,
  ],
  declarations: [
      AppComponent,
      CallbackComponent,
      LoginComponent,
      HomeComponent,
      SearchComponent,
      QuestionListComponent,
      QuestionComponent,
  ],
  providers: [
      AuthService,
      SearchService,
      AnswerService,
  ],
  bootstrap: [
      AppComponent,
  ],
})
export class AppModule {
}