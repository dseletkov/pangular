import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

export interface IQuestion {
  accepted_answer_id?: number;
  answer_count: number;
  creation_date: number;
  is_answered: boolean;
  last_activity_date: number;
  link: string;
  owner: {
    reputation: number,
    user_id: number,
    user_type: string,
    profile_image: string,
    display_name: string,
    link: string,
    [key: string]: any;
  };
  question_id: number;
  score: number;
  tags: string[];
  title: string;
  view_count: number;
  [key: string]: any;
}

export interface ISearchResponse<T> {
  items: T[];
  has_more: boolean;
  quota_max: number;
  quota_remaining: number;
}

@Injectable()
export class SearchService {

  constructor(private http: HttpClient) {

  }

  getQuestions(query: string): Observable<ISearchResponse<IQuestion>> {
    const queryUrl = `http://api.stackexchange.com/2.2/search?order=desc&sort=activity&intitle=${query}&site=stackoverflow`;

    return this.http.get<ISearchResponse<IQuestion>>(queryUrl);
  }

  getQuestionsByTag(tag: string): Observable<ISearchResponse<IQuestion>> {
    const queryUrl = `http://api.stackexchange.com/2.2/search?order=desc&sort=activity&tagged=${tag}&site=stackoverflow`;

    return this.http.get<ISearchResponse<IQuestion>>(queryUrl);
  }

  getQuestionsByUser(userId: number): Observable<ISearchResponse<IQuestion>> {
    const queryUrl = `http://api.stackexchange.com/2.2/users/${userId}/questions?order=desc&sort=votes&site=stackoverflow`;

    return this.http.get<ISearchResponse<IQuestion>>(queryUrl);
  }
}
