import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  SearchService,
  ISearchResponse,
  IQuestion,
} from './search.service';

@Component({
  templateUrl: './search.component.html',
  styleUrls: [ './search.component.scss' ],
})
export class SearchComponent implements OnInit {
  private query: string;
  private questions: IQuestion[];
  private quickQuestions: IQuestion[];
  private quickMessage: string;

  private static domParser = new DOMParser();
  public static decodeHtml(encoded: string): string {
    return SearchComponent.domParser.parseFromString(`<!doctype html><body>${encoded}`, 'text/html')
        .body.textContent;
  }
  public static decodeQuestion(question: IQuestion): IQuestion {
    return {
      ...question,
      title: SearchComponent.decodeHtml(question.title),
      owner: {
        ...question.owner,
        display_name: SearchComponent.decodeHtml(question.owner.display_name),
      },
    };
  }

  constructor(
      private activatedRoute: ActivatedRoute,
      private searchService: SearchService
  ) {}

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: any) => {
      this.query = params.query;
    });

    this.searchService.getQuestions(this.query).subscribe((response: ISearchResponse<IQuestion>) => {
      this.questions = response.items.map(SearchComponent.decodeQuestion);
    }, (error: any) => {
      console.warn(error.message || error.toString());
      this.questions = [];
    });
  }

  handleAuthorClick(id: number) {
    this.searchService.getQuestionsByUser(id).subscribe((response: ISearchResponse<IQuestion>) => {
      this.quickQuestions = response.items.map(SearchComponent.decodeQuestion);

      this.quickMessage = `Questions of user #${id}`;
      document.scrollingElement.scrollTo(0, 0);
    }, (error: any) => {
      console.warn(error.message || error.toString());
      this.quickQuestions = [];
    });
  }

  handleTagClick(tag: string) {
    this.searchService.getQuestionsByTag(tag).subscribe((response: ISearchResponse<IQuestion>) => {
      this.quickQuestions = response.items.map(SearchComponent.decodeQuestion);

      this.quickMessage = `Questions by tag ${tag}`;
      document.scrollingElement.scrollTo(0, 0);
    }, (error: any) => {
      console.warn(error.message || error.toString());
      this.quickQuestions = [];
    });
  }
}
