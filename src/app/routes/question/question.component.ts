import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AnswerService, IAnswer } from './answer.service';
import { SearchComponent } from '../search/search.component';

@Component({
  templateUrl: './question.component.html',
  styleUrls: [ './question.component.scss' ],
})
export class QuestionComponent implements OnInit {
  private questionTitle = 'Loading...';
  private answers: IAnswer[];

  constructor(private activatedRoute: ActivatedRoute,
              private answerService: AnswerService
  ) {}

  ngOnInit() {
    const questionId = +this.activatedRoute.snapshot.paramMap.get('id');

    this.answerService.getAnswersByQuestionId(questionId).subscribe((resp) => {
      if (!resp.items.length) {
        this.answers = [];
        this.questionTitle = '';
        return;
      }

      this.answers = resp.items.map((answer) => ({
          title: SearchComponent.decodeHtml(answer.title),
          body: answer.body,
      }));
      this.questionTitle = this.answers[0].title;
    }, (err) => {
      if (err) console.warn(err.message || err.toString());
      this.answers = [];
    });
  }
}