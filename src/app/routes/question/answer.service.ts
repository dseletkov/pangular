import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { ISearchResponse } from '../search/search.service';

export interface IAnswer {
  title: string;
  body: string;
  [key: string]: any;
}

@Injectable()
export class AnswerService {

  constructor(private http: HttpClient) {

  }

  getAnswersByQuestionId(id: number): Observable<ISearchResponse<IAnswer>> {
    const queryUrl = `http://api.stackexchange.com/2.2/questions/${id}/answers?order=desc&sort=activity&site=stackoverflow&filter=!-*jbN.OXKfDP`;

    return this.http.get<ISearchResponse<IAnswer>>(queryUrl);
  }
}
