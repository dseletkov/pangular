import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  templateUrl: './home.component.html',
  styleUrls: [ './home.component.scss' ],
})
export class HomeComponent  {
  query = '';

  constructor(private router: Router) {

  }

  onSubmit() {
    const query = this.query.trim();

    if (!query.length) {
      alert('Query should not be empty!');
      return;
    }

    this.router.navigate(['/search'], {
      queryParams: {
        query,
      },
    });
  }
}
