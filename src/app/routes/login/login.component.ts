import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  templateUrl: './login.component.html',
})
export class LoginComponent implements OnInit {
  constructor(public authService: AuthService,
              public router: Router) {}

  ngOnInit() {
    console.log(this.authService.isLoggedIn);
    if (this.authService.isLoggedIn) {
      this.router.navigate(['/']);
    } else {
      this.authService.login();
    }
  }
}