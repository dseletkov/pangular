import { Component, ViewEncapsulation } from '@angular/core';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap-grid.min.css';
import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-main',
  templateUrl: './app.component.html',
  styleUrls: [ './app.component.scss' ],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent {
  header = 'PAngular';

  constructor(public authService: AuthService) {}
}
