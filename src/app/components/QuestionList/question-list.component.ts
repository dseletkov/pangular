import {
  Component,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import { IQuestion } from '../../routes/search/search.service';

@Component({
  selector: 'question-list',
  templateUrl: './question-list.component.html',
  styleUrls: [ './question-list.component.scss' ],
})
export class QuestionListComponent {
  @Input() questions: IQuestion[];
  @Output() clickAuthor = new EventEmitter<number>();
  @Output() clickTag = new EventEmitter<string>();

  onClickAuthor(userId: number) {
    this.clickAuthor.emit(userId);
  }

  onClickTag(tag: string) {
    this.clickTag.emit(tag);
  }
}