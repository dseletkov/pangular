export const environment = {
  production: false,
  auth: {
    clientID: 'TP4AOXRLKKc5TSjqLbxCTUCBdhOiFFWa',
    domain: 'dyasik.eu.auth0.com', // e.g., you.auth0.com
    audience: 'http://localhost:3003', // e.g., http://localhost:3001
    redirect: 'http://localhost:3003/callback',
    scope: 'openid profile email',
  },
};